import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import SwaggerClient from 'swagger-client';
import { Route, Switch, BrowserRouter, Link } from "react-router-dom";
import { Navbar, NavbarBrand, Nav, NavItem, NavLink, Input, FormGroup, Label, ListGroup, ListGroupItem, Form } from "reactstrap";
import { Button } from 'react-bootstrap';

function App() {


  let client = null;

  new SwaggerClient("http://localhost:9999/swagger.json").then(result => {
    client = result;
    console.log(result)
  })

  function Search() {
    const [searchValue, setSearchValue] = useState("")
    const [items, setItems] = useState([])

    const search = (event) => {
      event.preventDefault();
      client.apis.IHardwareBoxService.post_IHardwareBoxService_GetItems({itemName: searchValue}).then((result) => {
        console.log(result)
        setItems(result.body.Items)
      })
    }

    const buy = (id) => {
      client.apis.IHardwareBoxService.post_IHardwareBoxService_BuyItem({itemId: id}).then((result) => {
        client.apis.IHardwareBoxService.post_IHardwareBoxService_GetItems({itemName: searchValue}).then((result) => {
          console.log(result)
          setItems(result.body.Items)
        })
      })
    }

    return (<div className="search-page">
      <Label for="exampleEmail">Search</Label>
      <Form onSubmit={search}>
        <Input type="text" name="search" id="search" placeholder="Enter item name..." value={searchValue} onChange={(event) => setSearchValue(event.target.value)} />
      </Form>
      <ListGroup className="items">
      {items.map((item, index) => (
          <ListGroupItem key={item.Id} className="item">
            {item.Name} 
            <span className="item-description">{item.Description}</span> 
            <Button className="item-buy" color="alert" onClick={() => buy(item.Id)}>Buy</Button></ListGroupItem>
      ))}
      </ListGroup>

    </div>)
  }

  return (
    <BrowserRouter>
      <Navbar color="dark" dark expand="md">
        <NavbarBrand tag={Link} to="/">Magazik</NavbarBrand>
        <Nav>
          <NavItem>
            <NavLink tag={Link} to="/">Home</NavLink>
          </NavItem>
          <NavItem>
            <NavLink tag={Link} to="/search">Search</NavLink>
          </NavItem>
        </Nav>
      </Navbar>
      <div className="page">
        <Switch>
          <Route path="/search">
            <Search />
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
}



export default App;
