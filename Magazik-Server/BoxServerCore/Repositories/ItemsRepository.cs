﻿using BoxProtocol.Models;
using BoxServerCore.Database;
using System.Collections.Generic;
using System.Linq;

namespace BoxServerCore.Repositories
{
    public static class ItemsRepository
    {
        #region private members

        /// <summary>
        /// Database context.
        /// </summary>
        private static readonly DatabaseContext databaseContext;

        #endregion

        #region public members

        /// <summary>
        /// Constructor.
        /// </summary>
        static ItemsRepository()
        {
            databaseContext = new DatabaseContext();
        }

        /// <summary>
        /// Get all items stored in database.
        /// </summary>
        /// <returns></returns>
        public static List<Item> GetItems()
        {
            return databaseContext.Items.ToList();
        }

        /// <summary>
        /// Add new item to database.
        /// </summary>
        /// <param name="item"></param>
        public static void AddItem(Item item)
        {
            using DatabaseContext db = new DatabaseContext();
            db.Items.Add(item);
            db.SaveChanges();
        }

        /// <summary>
        /// Remove item from database.
        /// </summary>
        /// <param name="item"></param>
        public static void Remove(Item item)
        {
            using DatabaseContext db = new DatabaseContext();
            db.Items.Remove(item);
            db.SaveChanges();
        }

        #endregion
    }
}