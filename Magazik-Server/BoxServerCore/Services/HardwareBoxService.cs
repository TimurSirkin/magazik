﻿using BoxProtocol.Enums;
using BoxProtocol.Interfaces;
using BoxProtocol.Messages;
using BoxProtocol.Models;
using BoxServerCore.Database;
using BoxServerCore.Repositories;
using MagicOnion;
using MagicOnion.Server;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace BoxServerCore.Services
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class HardwareBoxService : ServiceBase<IHardwareBoxService>, IHardwareBoxService
    {

        #region private members

        private readonly ILogger<Startup> logger;

        #endregion

        #region public members

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="logger"></param>
        public HardwareBoxService(ILogger<Startup> logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public UnaryResult<ItemsResponse> AddItem(string name, string description)
        {
            logger.LogInformation("Adding items...");
            try
            {
                if (name == null || name == string.Empty)
                {
                    throw new Exception();
                }

                ItemsRepository.AddItem(
                new Item
                {
                    Name = name,
                    Description = description
                });

                return new UnaryResult<ItemsResponse>(new ItemsResponse()
                {
                    Code = StatusCode.Ok,
                    Items = ItemsRepository.GetItems()
                }); ;
            }
            catch
            {
                logger.LogError("Error occurred during adding items...");
                return new UnaryResult<ItemsResponse>(new ItemsResponse()
                {
                    Code = StatusCode.Error,
                    Items = ItemsRepository.GetItems()
                });
            }

        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public UnaryResult<ItemsResponse> BuyItem(int itemId)
        {
            logger.LogInformation("Buying items...");
            try
            {
                ItemsRepository.Remove(new Item { Id = itemId });

                return new UnaryResult<ItemsResponse>(new ItemsResponse()
                {
                    Code = StatusCode.Ok,
                    Items = ItemsRepository.GetItems()
                });
            }
            catch
            {
                logger.LogError("Error occurred during buying items...");
                return new UnaryResult<ItemsResponse>(new ItemsResponse()
                {
                    Code = StatusCode.Error,
                    Items = ItemsRepository.GetItems()
                });
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public UnaryResult<ItemsResponse> GetItems(string itemName)
        {
            logger.LogInformation("Getting items...");
            try
            {
                if (itemName == null || itemName == string.Empty)
                {
                    return new UnaryResult<ItemsResponse>(new ItemsResponse()
                    {
                        Code = StatusCode.Ok,
                        Items = ItemsRepository.GetItems()
                    });
                }
                return new UnaryResult<ItemsResponse>(new ItemsResponse()
                {
                    Items = ItemsRepository.GetItems().Where(i =>
                    i.Name.ToLower().Contains(itemName.ToLower())
                    || i.Description.ToLower().Contains(itemName.ToLower())).ToList()
                });
            }
            catch
            {
                logger.LogError("Error occurred during getting items...");
                return new UnaryResult<ItemsResponse>(ItemsResponse.ErrorResponse());
            }
        }

        #endregion
    }
}