﻿using BoxProtocol.Models;
using Microsoft.EntityFrameworkCore;

namespace BoxServerCore.Database
{
    /// <summary>
    /// Database context.
    /// </summary>
    public class DatabaseContext : DbContext
    {
        /// <summary>
        /// List of stored items.
        /// </summary>
        public DbSet<Item> Items { get; set; }

        /// <summary>
        /// Creates new or connects to exist database.
        /// </summary>
        public DatabaseContext()
        {
            Database.EnsureCreated();
        }

        /// <summary>
        /// Connect to specified database. Will be called during new instance creation.
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=MagazikDB;Trusted_Connection=True;");
        }
    }
}