﻿using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Grpc.Core;
using MagicOnion;
using MagicOnion.Hosting;
using MagicOnion.HttpGateway.Swagger;
using MagicOnion.Server;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace BoxServerCore
{
    internal class Program
    {
        private static async Task Main()
        {

            var magicOnionHost = MagicOnionHost.CreateDefaultBuilder()
                .UseMagicOnion(
                    new MagicOnionOptions(true),
                    new ServerPort("127.0.0.1", 12345, ServerCredentials.Insecure)
                )
                .UseConsoleLifetime()
                .Build();

            var webHost = new WebHostBuilder()
                .ConfigureServices(
                    collection =>
                    {
                        collection
                        .AddCors(options =>
                        {
                            options.AddPolicy(name: "MyAllowSpecificOrigins",
                                              builder =>
                                              {
                                                  builder.WithOrigins("http://localhost",
                                                                      "http://localhost:10001",
                                                                      "http://localhost:3000",
                                                                      "http://localhost:9999",
                                                                      "*");
                                              });
                        })
                        .AddSingleton(
                            magicOnionHost.Services.GetService<MagicOnionHostedServiceDefinition>().ServiceDefinition
                        );

                        collection.Configure<KestrelServerOptions>(options => { options.AllowSynchronousIO = true; });
                    }
                )
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseStartup<Startup>()
                .UseUrls("http://0.0.0.0:9999")
                .Build();

            await Task.WhenAll(webHost.RunAsync(), magicOnionHost.RunAsync());
        }
    }

    // WebAPI Startup configuration.
    public class Startup
    {
        // Inject MagicOnionServiceDefinition from DIl
        public void Configure(IApplicationBuilder app, MagicOnionServiceDefinition magicOnion)
        {
            app.UseCors("MyAllowSpecificOrigins");

            var xmlName = "Sandbox.NetCoreServer.xml";
            var xmlPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), xmlName);
            app.UseStaticFiles();

            app.UseMagicOnionSwagger(
                magicOnion.MethodHandlers,
                new SwaggerOptions("MagicOnion.Server", "Swagger Integration Test", "/") {
                    XmlDocumentPath = xmlPath
                }
            );
            app.UseMagicOnionHttpGateway(
                magicOnion.MethodHandlers,
                new Channel("127.0.0.1:12345", ChannelCredentials.Insecure)
            );
        }
    }
}
