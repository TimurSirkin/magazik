﻿using BoxProtocol.Enums;
using BoxProtocol.Models;
using MessagePack;
using System.Collections.Generic;

namespace BoxProtocol.Messages
{
    /// <summary>
    /// Items response. Used for gRPC communication.
    /// </summary>
    [MessagePackObject]
    public class ItemsResponse
    {
        /// <summary>
        /// Status code.
        /// </summary>
        [Key(0)]
        public StatusCode Code { get; set; }

        /// <summary>
        /// List of items to show.
        /// </summary>
        [Key(1)]
        public List<Item> Items { get; set; }

        /// <summary>
        /// Creates empty ItemResponse instance with error code inside.
        /// </summary>
        /// <returns></returns>
        public static ItemsResponse ErrorResponse()
        {
            return new ItemsResponse()
            {
                Code = StatusCode.Error,
                Items = new List<Item>()
            };
        }
    }
}