﻿namespace BoxProtocol.Interfaces
{
    /// <summary>
    /// Interface of identified objecs.
    /// </summary>
    public interface IIdentifieted
    {
        public int Id { get; set; }
    }
}