﻿using BoxProtocol.Messages;
using MagicOnion;

namespace BoxProtocol.Interfaces
{
    /// <summary>
    /// Main service interface. Allows manage items.
    /// </summary>
    public interface IHardwareBoxService : IService<IHardwareBoxService>
    {
        /// <summary>
        /// Get all items, which name or description contains specified itemName.
        /// </summary>
        /// <param name="itemName"></param>
        /// <returns></returns>
        UnaryResult<ItemsResponse> GetItems(string itemName);

        /// <summary>
        /// Buy specified item.
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        UnaryResult<ItemsResponse> BuyItem(int itemId);

        /// <summary>
        /// Add specified item.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        UnaryResult<ItemsResponse> AddItem(string name, string description);
    }
}