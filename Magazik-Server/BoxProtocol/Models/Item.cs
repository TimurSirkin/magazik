﻿using BoxProtocol.Interfaces;
using System;
using MessagePack;
using System.Diagnostics.CodeAnalysis;

namespace BoxProtocol.Models
{
    /// <summary>
    /// Item. It may be whatever you want =)
    /// </summary>
    [MessagePackObject]
    public class Item : IEquatable<Item>, IIdentifieted
    {
        /// <summary>
        /// Identifier.
        /// </summary>
        [Key(0)]
        public int Id { get; set; }

        /// <summary>
        /// Name.
        /// </summary>
        [Key(1)]
        public string Name { get; set; }

        /// <summary>
        /// Description.
        /// </summary>
        [Key(2)]
        public string Description { get; set; }

        /// <summary>
        /// Equals overridden method. Allows to compare items by Id.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals([AllowNull] Item other)
        {
            if (other == null)
            {
                return false;
            }

            return (Id == other.Id);
        }
    }
}