import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import SwaggerClient from 'swagger-client';
import { Navbar, NavbarBrand, Nav, NavItem, NavLink, InputGroup, Input, InputGroupAddon, InputGroupText, ListGroup, ListGroupItem, Label } from "reactstrap";
import Form from 'reactstrap/lib/Form';
import Button from 'reactstrap/lib/Button';


class App extends React.Component
{

  constructor(props){
    super(props);
    this.state = {
      client: null,
      items: []
    }

  }

  componentDidMount() {
    new SwaggerClient("http://localhost:9999/swagger.json").then(result => {
      this.setState({ client: result })
      console.log(result)
      result.apis.IHardwareBoxService.post_IHardwareBoxService_GetItems({ itemName: "" }).then((result) => {
        this.setState({items: result.body.Items})
      })
    })
  }

  render() {
    const Page = () => {
      const [name, setName] = useState("");
      const [description, setDescription] = useState("");


      const add = () => {
        if (this.state.client)
          this.state.client.apis.IHardwareBoxService.post_IHardwareBoxService_AddItem({ name: name, description: description }).then(result => {
            this.setState({items: result.body.Items})
          })
      }

      return (
        <><Form>
          <Label>Add item</Label>
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <InputGroupText>Item name</InputGroupText>
            </InputGroupAddon>
            <Input placeholder="Item name" value={name} onChange={(event) => setName(event.target.value)} />
          </InputGroup>
          <br />
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <InputGroupText>Item description</InputGroupText>
            </InputGroupAddon>
            <Input placeholder="Item description" value={description} onChange={(event) => setDescription(event.target.value)} />
          </InputGroup>
          <br />
          <Button color="success" onClick={add}>Add</Button>
        </Form>
        <hr></hr>
        <Label className = "existItemsLabel">Exist items</Label>
            <ListGroup className="items">
              {this.state.items.map((item, index) => (
                <ListGroupItem key={index} className="item">
                  {item.Name}
                  <span className="item-description">{item.Description}</span>
                </ListGroupItem>
              ))}
            </ListGroup>
        </>)
    }

    return (
      <div className="App" >
        <Navbar color="dark" dark expand="md">
          <NavbarBrand>Magazik-Admin</NavbarBrand>
        </Navbar>
        <div className="page">
          <Page />
        </div>

      </div>
    );
  }

}

export default App;
